Connected Speech
================

Introduction
------------

Instructional tool for teachers and students to visualise linking words in English connected speech.

A student can copy and paste English text into the web page and see a linked-speech visualisation showing where and how individual works should be pronounced together to replicate native speaker speech patterns.

A short user guide and explaination of the the connected speech concept can be found in the `doc/` directory.

*Try it out!*  The latest version is available at: https://scotek.gitlab.io/connected-speech

Development homepage: https://gitlab.com/scotek/connected-speech

Usage
-----

The system works entirely in JavaScript in the browser and does not need any server component.  The files required are:

- `connected-dictionary.js`
- `connected-speech.html`

To run from a web server, copy these files above to the server and open `connected-speech.html` in a web browser.

You can also run the tool from the local file-system without a web server.  Just open the `connected-speech.html`.

Credits
-------

The original idea is by  Michael Youles.
The original program is by Patrick McCarthy.
Further development by Paul Dempster.

License
-------

(c) 2016-2018  Patrick McCarthy, Michael Youles.
(c) 2018 Paul Dempster.

LPGL v3.  See LICENSE.txt for the full license text.
